-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2021 at 04:04 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop24h`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rate_star` decimal(19,2) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comments`, `name`, `rate_star`, `customer_id`, `product_id`) VALUES
(1, 'Hàng đẹp giao hơi chậm', 'Nguyễn Mạnh Tường', '4.50', 4, 1),
(2, 'Hàng đẹp', 'Nguyễn Thúy My', '5.00', 10, 11),
(3, 'tôi rất thích', 'Nguyễn Vân Anh', '4.50', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `credit_limit` int(11) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `sales_rep_employee_number` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `created_at`, `created_by`, `deleted`, `updated_at`, `updated_by`, `address`, `city`, `country`, `credit_limit`, `first_name`, `last_name`, `password`, `phone_number`, `postal_code`, `sales_rep_employee_number`, `state`, `username`) VALUES
(2, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 'Ngô Hoàng Hữu', 'Tài', '$2a$10$9GDmP31zpIgmSYy83Aobw.jUeoXEsfd1VEO.w6qvbSUCL4hZxdk9G', 'Admin', NULL, 0, NULL, 'Admin'),
(3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 'Nguyễn Minh', 'Tuấn', '$2a$10$JxyeoqGn7ou.TWNAwiWuqOGPznbxm1CXQQZSs1MogFqr3P70WQzDK', 'minhtuan', NULL, 0, NULL, 'minhtuan'),
(4, NULL, NULL, 0, NULL, NULL, '05 Hà Huy Tập', 'Hồ Chí Minh', 'Việt Nam', 0, 'Nguyễn Mạnh', 'Tường', '$2a$10$uGMg6pZk.HGJbxbuL8h9j.x9g93IjAwjB7jENO73qyzxfSRifCCPa', '0999777888', '', 0, 'Hồ Chí Minh', '0999777888'),
(5, NULL, NULL, 0, NULL, NULL, '03 Nguyễn Tất Thành', 'Hồ Chí Minh', 'Việt Nam', 0, 'Nguyễn Minh', 'Huy', '$2a$10$IXUOA1pNw/XexORwZVHnvOGqLiOmgLR92gmDWh12snw34kk91hgQ6', '0987987987', '', 0, 'Hồ Chí Minh', '0987987987'),
(7, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 'Ngô Thị', 'My', '$2a$10$XSf./l2Ik74v4lczVDdlW.t8lBU20ckeQhTTECsTvIq01oMCXuG/a', '0877778779', NULL, 0, NULL, '0877778779'),
(8, NULL, NULL, 0, NULL, NULL, '03 Nguyễn Tất Thành', 'Hồ Chí Minh', 'Việt Nam', 0, 'Nguyễn Vân', 'Anh', '$2a$10$LiQfGYKQ7eLdHKIZhmmbguw4B2ch9pYdEdjLMXBGCeda3pxtIdfR6', '0996666777', '', 0, 'Hồ Chí Minh', '0996666777'),
(9, NULL, NULL, 0, NULL, NULL, '03 Nguyễn Tất Thành', 'Hồ Chí Minh', 'Việt Nam', 0, 'Nguyễn Hà Minh', 'Huy', '$2a$10$LiQfGYKQ7eLdHKIZhmmbguw4B2ch9pYdEdjLMXBGCeda3pxtIdfR6', '09963332211', '', 0, 'Hồ Chí Minh', '09963332211'),
(10, NULL, NULL, 0, NULL, NULL, '', '', '', 0, 'Nguyễn Thúy', 'My', '$2a$10$LiQfGYKQ7eLdHKIZhmmbguw4B2ch9pYdEdjLMXBGCeda3pxtIdfR6', '0976654321', '', 0, '', '0976654321');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `office_code` int(11) NOT NULL,
  `report_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` int(11) NOT NULL,
  `address_line` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `territory` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `order_date` date NOT NULL,
  `required_date` date NOT NULL,
  `shipped_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `comments`, `order_date`, `required_date`, `shipped_date`, `status`, `customer_id`) VALUES
(1, '', '2021-09-10', '2021-09-25', NULL, 'open', 4),
(2, '', '2021-09-11', '2021-09-09', NULL, 'open', 5),
(3, '', '2021-09-12', '2021-09-22', NULL, 'open', 7),
(4, '', '2021-09-13', '2021-09-25', NULL, 'open', 8),
(5, '', '2021-09-14', '2021-09-07', NULL, 'open', 9),
(6, '', '2021-09-15', '2021-09-18', NULL, 'open', 10),
(7, '', '2021-09-16', '2021-10-02', NULL, 'open', 4),
(8, '', '2021-09-17', '2021-09-16', NULL, 'open', 5),
(9, '', '2021-09-18', '2021-09-17', NULL, 'open', 7),
(10, '', '2021-09-19', '2021-09-23', NULL, 'open', 8),
(11, '', '2021-09-20', '2021-09-16', NULL, 'open', 9),
(12, '', '2021-09-10', '2021-09-15', NULL, 'open', 10),
(13, '', '2021-09-11', '2021-09-25', NULL, 'open', 4),
(14, '', '2021-09-12', '2021-09-16', NULL, 'open', 5),
(15, '', '2021-09-13', '2021-09-18', NULL, 'open', 7),
(16, '', '2021-09-14', '2021-09-22', NULL, 'open', 8),
(17, '', '2021-09-15', '2021-09-16', NULL, 'open', 9),
(18, '', '2021-09-16', '2021-09-25', NULL, 'open', 10),
(19, '', '2021-09-17', '2021-09-25', NULL, 'open', 4),
(20, '', '2021-09-18', '2021-10-01', NULL, 'open', 5),
(21, '', '2021-09-19', '2021-09-27', NULL, 'open', 7),
(22, '', '2021-09-20', '2021-09-27', NULL, 'open', 8),
(23, '', '2021-09-10', '2021-09-30', NULL, 'open', 9),
(24, '', '2021-09-11', '2021-09-22', NULL, 'open', 10),
(25, '', '2021-09-12', '2021-10-01', NULL, 'open', 4),
(26, '', '2021-09-13', '2021-09-25', NULL, 'open', 5),
(27, '', '2021-09-14', '2021-10-07', NULL, 'open', 8);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `price_each` decimal(19,2) NOT NULL,
  `quantity_order` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `price_each`, `quantity_order`, `order_id`, `product_id`) VALUES
(1, '459000.00', 1, 1, 1),
(2, '814000.00', 1, 1, 3),
(3, '387000.00', 1, 2, 2),
(4, '1407000.00', 2, 3, 5),
(5, '814000.00', 1, 4, 3),
(6, '1262000.00', 1, 4, 6),
(7, '814000.00', 1, 5, 3),
(8, '992000.00', 1, 5, 4),
(9, '475000.00', 1, 6, 11),
(10, '450000.00', 1, 6, 9),
(11, '459000.00', 1, 7, 1),
(12, '1407000.00', 1, 7, 5),
(13, '459000.00', 1, 8, 1),
(14, '814000.00', 1, 8, 3),
(15, '814000.00', 2, 9, 3),
(16, '1407000.00', 1, 10, 5),
(17, '479000.00', 1, 10, 12),
(18, '525000.00', 1, 11, 16),
(19, '499000.00', 1, 11, 18),
(20, '1407000.00', 1, 12, 5),
(21, '1262000.00', 1, 12, 6),
(22, '814000.00', 1, 13, 3),
(23, '1262000.00', 1, 13, 6),
(24, '387000.00', 1, 14, 2),
(25, '475000.00', 1, 14, 11),
(26, '1262000.00', 1, 15, 6),
(27, '459000.00', 1, 16, 1),
(28, '814000.00', 1, 17, 3),
(29, '1262000.00', 1, 17, 6),
(30, '475000.00', 1, 18, 11),
(31, '357000.00', 1, 18, 7),
(32, '306000.00', 1, 19, 13),
(33, '367000.00', 1, 19, 15),
(34, '814000.00', 1, 20, 3),
(35, '992000.00', 1, 20, 4),
(36, '1407000.00', 1, 21, 5),
(37, '1262000.00', 1, 21, 6),
(38, '357000.00', 1, 22, 7),
(39, '328000.00', 1, 22, 8),
(40, '450000.00', 1, 23, 9),
(41, '650000.00', 1, 23, 10),
(42, '475000.00', 1, 24, 11),
(43, '479000.00', 1, 24, 12),
(44, '306000.00', 1, 25, 13),
(45, '505000.00', 1, 25, 14),
(46, '367000.00', 1, 26, 15),
(47, '525000.00', 1, 26, 16),
(48, '365000.00', 1, 27, 17),
(49, '499000.00', 1, 27, 18);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `ammount` decimal(19,2) NOT NULL,
  `check_number` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `customer_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `ammount`, `check_number`, `payment_date`, `customer_id`) VALUES
(1, '15000000.00', '1', '2021-09-01', 4),
(2, '5000000.00', '2', '2021-09-01', 7),
(3, '12000000.00', '3', '2021-09-02', 7),
(4, '30000000.00', '4', '2021-09-02', 8),
(5, '12000000.00', '5', '2021-09-03', 9),
(6, '5000000.00', '6', '2021-09-03', 10),
(7, '4500000.00', '7', '2021-09-04', 4),
(8, '3500000.00', '8', '2021-09-04', 5),
(9, '8000000.00', '9', '2021-09-05', 7),
(10, '34000000.00', '10', '2021-09-05', 7),
(11, '15000000.00', '11', '2021-09-06', 8),
(12, '13000000.00', '12', '2021-09-07', 9),
(13, '3200000.00', '13', '2021-09-08', 10),
(14, '2600000.00', '14', '2021-09-09', 4),
(15, '17000000.00', '15', '2021-09-10', 5),
(16, '5000000.00', '16', '2021-09-11', 7),
(17, '12000000.00', '17', '2021-09-12', 7),
(18, '1200000.00', '18', '2021-09-13', 8),
(19, '6000000.00', '19', '2021-09-14', 9),
(20, '7000000.00', '20', '2021-09-15', 10),
(21, '6000000.00', '21', '2021-09-16', 4),
(22, '8500000.00', '22', '2021-09-17', 5),
(23, '9600000.00', '23', '2021-09-18', 7),
(24, '7500000.00', '24', '2021-09-19', 7),
(25, '3400000.00', '25', '2021-09-20', 8),
(26, '12000000.00', '26', '2021-09-21', 9),
(27, '12500000.00', '27', '2021-09-22', 10),
(28, '13000000.00', '28', '2021-09-23', 4),
(29, '6000000.00', '29', '2021-09-24', 5),
(30, '3000000.00', '30', '2021-09-10', 7),
(31, '3700000.00', '31', '2021-09-11', 7),
(32, '16000000.00', '32', '2021-09-17', 8),
(33, '3000000.00', '33', '2021-09-18', 9),
(34, '1000000.00', '34', '2021-09-19', 10),
(35, '6000000.00', '35', '2021-09-22', 5),
(36, '5000000.00', '36', '2021-09-14', 8);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `buy_price` decimal(19,2) NOT NULL,
  `is_related` bit(1) DEFAULT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_description` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_scale` varchar(255) NOT NULL,
  `product_vendor` varchar(255) NOT NULL,
  `quantity_in_stock` int(11) NOT NULL,
  `url_image` varchar(255) NOT NULL,
  `product_line_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `buy_price`, `is_related`, `product_code`, `product_description`, `product_name`, `product_scale`, `product_vendor`, `quantity_in_stock`, `url_image`, `product_line_id`) VALUES
(1, '459000.00', b'1', 'sw2108256458824841', 'Honeyspot Đầm Nơ trước Nút phía trước màu trơn Thanh lịch', 'Honeyspot Đầm Nơ trước Nút phía trước màu trơn Thanh lịch', 'good', 'SHEIN', 150, 'https://img.ltwebstatic.com/images3_pi/2021/08/29/163021970562926a2e3c34f197fde60efe2c79e83f_thumbnail_900x.webp', 2),
(2, '387000.00', b'1', 'sw2108284060061130', 'Honeyspot Đầm Hội Chữ thập Xù màu trơn Thanh lịch', 'Honeyspot Đầm Hội Chữ thập Xù màu trơn Thanh lịch', 'good', 'SHEIN', 200, 'https://img.ltwebstatic.com/images3_pi/2021/08/29/16302338225839aa2d03d18b1ec238f8884c4f9947_thumbnail_900x.webp', 2),
(3, '814000.00', b'1', 'sw2109023357003445', 'Đầm Lộ Lưng Tương phản ren Dây kéo màu trơn Gợi cảm', 'Đầm Lộ Lưng Tương phản ren Dây kéo màu trơn Gợi cảm', 'good', 'SHEIN', 250, 'https://img.ltwebstatic.com/images3_pi/2021/07/12/16260876639fb9783ccd4e2401631bfce36b8d9f79_thumbnail_900x.webp', 2),
(4, '992000.00', b'1', 'sw2108308755302907', 'ADYCE Đầm Thắt lưng Chia Dây kéo màu trơn Thanh lịch', 'ADYCE Đầm Thắt lưng Chia Dây kéo màu trơn Thanh lịch', 'good', 'SHEIN', 300, 'https://img.ltwebstatic.com/images3_pi/2021/09/01/1630479567ff3c2a1956ba5bf85884cca8a2704df8_thumbnail_900x.webp', 2),
(5, '1407000.00', b'1', 'sw2109031981006967', 'Missord Đầm Lộ Lưng Dây kéo màu trơn Hấp dẫn', 'Missord Đầm Lộ Lưng Dây kéo màu trơn Hấp dẫn', 'good', 'SHEIN', 300, 'https://img.ltwebstatic.com/images3_pi/2021/09/06/1630905732dae0e3c91ad6b81d68d0defa19a349c3_thumbnail_900x.webp', 2),
(6, '1262000.00', b'1', 'sw2109042302552231', 'Missord Đầm Buộc lại Tách cao màu trơn Hấp dẫn', 'Missord Đầm Buộc lại Tách cao màu trơn Hấp dẫn', 'good', 'SHEIN', 150, 'https://img.ltwebstatic.com/images3_pi/2021/09/16/1631789427f05c9b889f859b16608f86322850a5f2_thumbnail_900x.webp', 2),
(7, '357000.00', b'0', 'smshirt07210610356', 'SHEIN BASICS Áo sơ mi Nam Nút phía trước màu trơn Cơ bản', 'SHEIN BASICS Áo sơ mi Nam Nút phía trước màu trơn Cơ bản', 'good', 'SHEIN', 300, 'https://img.ltwebstatic.com/images3_pi/2021/08/16/16290842221f3e02c0271d8bf04d7c945156271171_thumbnail_900x.webp', 1),
(8, '328000.00', b'0', 'sm2108183049445034', 'SHEIN Áo sơ mi Nam Túi Ruy băng Nút phía trước màu trơn Giải trí', 'SHEIN Áo sơ mi Nam Túi Ruy băng Nút phía trước màu trơn Giải trí', 'good', 'SHEIN', 500, 'https://img.ltwebstatic.com/images3_pi/2021/09/16/163175968909cf6d1c72b082aac19dd5347200f5e6_thumbnail_900x.webp', 1),
(9, '450000.00', b'0', 'sm2106301942917137', 'SHEIN Áo sơ mi Nam Nút Túi Đồ họa Giải trí', 'SHEIN Áo sơ mi Nam Nút Túi Đồ họa Giải trí', 'good', 'SHEIN', 300, 'https://img.ltwebstatic.com/images3_pi/2021/08/27/163005019340631ae296571186d64c0e59cad597b6_thumbnail_900x.webp', 1),
(10, '650000.00', b'0', 'sm2108304104444050', 'Áo sơ mi Nam Nút phía trước Lá thư Sọc ca rô Giải trí', 'Áo sơ mi Nam Nút phía trước Lá thư Sọc ca rô Giải trí', 'good', 'SHEIN', 250, 'https://img.ltwebstatic.com/images3_pi/2021/09/06/1630904722ba5f60eb5f4771bbccd2d2b32391bf6b_thumbnail_900x.webp', 1),
(11, '475000.00', b'0', 'sm2107229834972634', 'SHEIN Áo sơ mi Nam Nút phía trước màu trơn Giải trí', 'SHEIN Áo sơ mi Nam Nút phía trước màu trơn Giải trí', 'good', 'SHEIN', 150, 'https://img.ltwebstatic.com/images3_pi/2021/08/24/1629770712d232953b1ea92c4cd8cf3c0ebe23328e_thumbnail_900x.webp', 1),
(12, '479000.00', b'0', 'sm2107304067145490', 'SHEIN Áo sơ mi Nam Nút phía trước màu trơn Giải trí', 'SHEIN Áo sơ mi Nam Nút phía trước màu trơn Giải trí', 'good', 'SHEIN', 200, 'https://img.ltwebstatic.com/images3_pi/2021/08/24/162976968622a4c81ccfe44f1f28478a7fcdfbc89c_thumbnail_900x.webp', 1),
(13, '306000.00', b'1', 'sk2108040911730001', 'SHEIN Áo nỉ Gái Nút Xù nhỏ Khối Màu Sọc Dễ thương', 'SHEIN Áo nỉ Gái Nút Xù nhỏ Khối Màu Sọc Dễ thương', 'good', 'SHEIN', 250, 'https://img.ltwebstatic.com/images3_pi/2021/09/03/1630637457ace6cd05421c2054a0c39c34b8992c73_thumbnail_900x.webp', 3),
(14, '505000.00', b'1', 'sk2108041877048835', 'SHEIN Bộ hai mảnh Gái Viên lá sen Nút phía trước Họa tiết hoa Dễ thương', 'SHEIN Bộ hai mảnh Gái Viên lá sen Nút phía trước Họa tiết hoa Dễ thương', 'good', 'SHEIN', 150, 'https://img.ltwebstatic.com/images3_pi/2021/09/06/163090550580ea98a349dad1c613857a35e9d624e3_thumbnail_900x.webp', 3),
(15, '367000.00', b'1', 'sk2108069786666728', 'SHEIN Đầm Gái Tương phản Mesh viền lá sen màu trơn Dễ thương', 'SHEIN Đầm Gái Tương phản Mesh viền lá sen màu trơn Dễ thương', 'good', 'SHEIN', 150, 'https://img.ltwebstatic.com/images3_pi/2021/09/03/163063775128dff25632451d91761984b702be9efa_thumbnail_900x.webp', 3),
(16, '525000.00', b'0', 'sk2107065827713915', 'SHEIN Bộ hai mảnh Trai Túi Dây kéo Khối Màu Lá thư Thể thao', 'SHEIN Bộ hai mảnh Trai Túi Dây kéo Khối Màu Lá thư Thể thao', 'good', 'SHEIN', 250, 'https://img.ltwebstatic.com/images3_pi/2021/08/09/1628479149c816c332199853bf42e96b063011a8f9_thumbnail_900x.webp', 3),
(17, '365000.00', b'0', 'sk2107273337442694', 'SHEIN Áo nỉ Trai Đắp vá Túi Dây kéo màu trơn Giải trí', 'SHEIN Áo nỉ Trai Đắp vá Túi Dây kéo màu trơn Giải trí', 'good', 'SHEIN', 145, 'https://img.ltwebstatic.com/images3_pi/2021/09/01/163046004301290d9fd5aa4f35a9e09e3ecec086d7_thumbnail_900x.webp', 3),
(18, '499000.00', b'1', 'sk2107262063465616', 'SHEIN Bộ hai mảnh Trai Túi Khối Màu Lá thư Thể thao', 'SHEIN Bộ hai mảnh Trai Túi Khối Màu Lá thư Thể thao', 'good', 'SHEIN', 150, 'https://img.ltwebstatic.com/images3_pi/2021/09/01/163046045897f39b52863a80bcf483128b08c435d1_thumbnail_900x.webp', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_lines`
--

CREATE TABLE `product_lines` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `product_line` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_lines`
--

INSERT INTO `product_lines` (`id`, `description`, `product_line`) VALUES
(1, 'Nam', 'Nam'),
(2, 'Nữ', 'Nữ'),
(3, 'Trẻ em', 'Trẻ em');

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(11) NOT NULL,
  `reply` varchar(255) NOT NULL,
  `comment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `replies`
--

INSERT INTO `replies` (`id`, `reply`, `comment_id`) VALUES
(1, 'Dạ cám ơn đã để lại đánh giá lần sau shop sẽ giao nhanh hơn', 1),
(2, 'Cám ơn bạn đã đánh giá sản phẩm', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_permission`
--

CREATE TABLE `t_permission` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `permission_key` varchar(255) DEFAULT NULL,
  `permission_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_permission`
--

INSERT INTO `t_permission` (`id`, `created_at`, `created_by`, `deleted`, `updated_at`, `updated_by`, `permission_key`, `permission_name`) VALUES
(1, '2021-09-23 05:46:15', NULL, 0, '2021-09-23 05:46:15', NULL, 'CREATE', 'Create'),
(2, '2021-09-23 05:46:15', NULL, 0, '2021-09-23 05:46:15', NULL, 'READ', 'Read'),
(3, '2021-09-23 05:46:15', NULL, 0, '2021-09-23 05:46:15', NULL, 'UPDATE', 'Update'),
(4, '2021-09-23 05:46:15', NULL, 0, '2021-09-23 05:46:15', NULL, 'DELETE', 'Delete');

-- --------------------------------------------------------

--
-- Table structure for table `t_role`
--

CREATE TABLE `t_role` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `role_key` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_role`
--

INSERT INTO `t_role` (`id`, `created_at`, `created_by`, `deleted`, `updated_at`, `updated_by`, `role_key`, `role_name`) VALUES
(1, '2021-09-23 05:41:39', NULL, 0, '2021-09-23 05:41:39', NULL, 'ROLE_ADMIN', 'Admin'),
(2, '2021-09-23 05:41:39', NULL, 0, '2021-09-23 05:41:39', NULL, 'ROLE_MANAGER', 'Manager'),
(3, '2021-09-23 05:41:39', NULL, 0, '2021-09-23 05:41:39', NULL, 'ROLE_CUSTOMER', 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `t_role_permission`
--

CREATE TABLE `t_role_permission` (
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_role_permission`
--

INSERT INTO `t_role_permission` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 1),
(2, 2),
(2, 3),
(3, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_token`
--

CREATE TABLE `t_token` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL,
  `token_exp_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_token`
--

INSERT INTO `t_token` (`id`, `created_at`, `created_by`, `deleted`, `updated_at`, `updated_by`, `token`, `token_exp_date`) VALUES
(1, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMyMzM0MDMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0.Ng0JGM7fCLhabl0MpsGESJzNTuIq8TsiDDTJRaqg4z4', '2021-10-03 10:56:43'),
(2, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(3, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.bBXFKyoLf7t0CCo2I-iwL1wb0-hsjXxjMA9zGlLJkTg', '2021-10-04 08:10:12'),
(4, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.bBXFKyoLf7t0CCo2I-iwL1wb0-hsjXxjMA9zGlLJkTg', '2021-10-04 08:10:12'),
(5, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.bBXFKyoLf7t0CCo2I-iwL1wb0-hsjXxjMA9zGlLJkTg', '2021-10-04 08:10:12'),
(6, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(7, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(8, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(9, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(10, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.bBXFKyoLf7t0CCo2I-iwL1wb0-hsjXxjMA9zGlLJkTg', '2021-10-04 08:10:12'),
(11, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.bBXFKyoLf7t0CCo2I-iwL1wb0-hsjXxjMA9zGlLJkTg', '2021-10-04 08:10:12'),
(12, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(13, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(14, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(15, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(16, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0._HRXvKvWJ_hA6hRLTbGZoebpQtWJF9gCHyONrhA6EvE', '2021-10-04 08:10:12'),
(17, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk4MjMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.VKS4uS0KXZVH2GoL0v0ApEa_yhhZNizD9Tjq4iEY-2g', '2021-10-04 08:10:23'),
(18, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk5NDEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkekR2TVZjZnN3WmU5Q2prNHZjdFRzZTNjWENRamVcL2hRcVwvLktYd3liaXJmUDM4aTlcL3U4U0ciLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.dUyFBe3Owiz0GHSQMtZiUuA74YeJ-k1RrG9rUR95W2c', '2021-10-04 08:12:21'),
(19, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk5NDksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkekR2TVZjZnN3WmU5Q2prNHZjdFRzZTNjWENRamVcL2hRcVwvLktYd3liaXJmUDM4aTlcL3U4U0ciLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.C388EAzBT9YgQtWeyKK263rZi_jsprgq0ONxLAxklu4', '2021-10-04 08:12:29'),
(20, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMDk5NzcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdUdNZzZwWmsuSEdKYnhidUw4aDlqLng5ZzkzSWpBd2pCN2pFTk83M3F5enhmU1JpZkNDUGEiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.lwy1ABGVuTompvhxdDvhxN-6tXC4GkNA76SA5m9XxuY', '2021-10-04 08:12:57'),
(21, NULL, 5, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTAwNDksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk4Nzk4Nzk4NyJ9fQ.xSCwxKc1FYazS07qyYz1xg20D31wroP9xUG9WrK9kNE', '2021-10-04 08:14:09'),
(22, NULL, 5, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTAwNzQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaGhMVGNVUnk4M3FcL2lJd2xvNTFqcy5HUkVuQXpyNWZGYkJoODVySzNKZkVjRFBDcVlISmd1IiwidXNlcklkIjo1LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIiwiUkVBRCIsIlVQREFURSJdLCJ1c2VybmFtZSI6IjA5ODc5ODc5ODcifX0.UldGhu_BKd70nDKEXuDO8o9BhYz1_RRKERQ3IUCeh70', '2021-10-04 08:14:34'),
(23, NULL, 5, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTAwODUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaGhMVGNVUnk4M3FcL2lJd2xvNTFqcy5HUkVuQXpyNWZGYkJoODVySzNKZkVjRFBDcVlISmd1IiwidXNlcklkIjo1LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIiwiUkVBRCIsIlVQREFURSJdLCJ1c2VybmFtZSI6IjA5ODc5ODc5ODcifX0.94A4tpgXB0_5NC186sjrO5tnF772JUI5rfytCRS0nCY', '2021-10-04 08:14:45'),
(24, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTAxMjcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0.mlSnU6m_L83zTerlUmOIRijDTFEKCKLRokTvcF28-gY', '2021-10-04 08:15:27'),
(25, NULL, 3, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTAxNDYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkSnh5ZW9xR243b3UuVFdOQXdpV3VxT0dQem5ieG0xQ1hRUVpTczFNb2dGcXIzUDcwV1F6REsiLCJ1c2VySWQiOjMsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJDUkVBVEUiLCJST0xFX01BTkFHRVIiLCJVUERBVEUiXSwidXNlcm5hbWUiOiJtaW5odHVhbiJ9fQ.aaJ5QrOKNV6gjieG2ttzoHGRGYFTbzQLlCo6YkmK30E', '2021-10-04 08:15:46'),
(26, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTAxOTUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0.c2fw7AnsEmSxtWWk06fQroGoAKTa_63hTilmrTteCag', '2021-10-04 08:16:35'),
(27, NULL, 5, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTA0NTMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkSVhVT0ExcE53XC9YZXhPUndaVkhudk9HcUxpT21nTFI5MmdtRFdoMTJzbnczNGtrOTFoZ1E2IiwidXNlcklkIjo1LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIiwiUkVBRCIsIlVQREFURSJdLCJ1c2VybmFtZSI6IjA5ODc5ODc5ODcifX0.GsVXrz0TEKbRjcE0hBg0IES5pKbAGBdiMnFbUrOWCfE', '2021-10-04 08:20:53'),
(28, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTA2NDksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0.RZsLSWVl16svQipKB5aSK5DwbIQ_yCMwDsbahbskuqA', '2021-10-04 08:24:09'),
(29, NULL, 7, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzMTEyNjgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWFNmLlwvbDJJazc0djRsY3pWRGRsVy50OGxCVTIwY2tlUWhUVEVDc1R2SXEwMW9NQ1h1R1wvYSIsInVzZXJJZCI6NywiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiIsIlJFQUQiLCJVUERBVEUiXSwidXNlcm5hbWUiOiIwODc3Nzc4Nzc5In19.1JqbQHfup1jSte3QCf9il_UKCb2sknLcBAJCrVGA6J8', '2021-10-04 08:34:28'),
(30, NULL, 4, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzOTg0MjIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdUdNZzZwWmsuSEdKYnhidUw4aDlqLng5ZzkzSWpBd2pCN2pFTk83M3F5enhmU1JpZkNDUGEiLCJ1c2VySWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5OTc3Nzg4OCJ9fQ.FHjs9SoV4NDdmj0yn4PLfdqq8ZzKOIzEeRq_biuiBw4', '2021-10-05 08:47:02'),
(31, NULL, 5, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzOTg0NjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkSVhVT0ExcE53XC9YZXhPUndaVkhudk9HcUxpT21nTFI5MmdtRFdoMTJzbnczNGtrOTFoZ1E2IiwidXNlcklkIjo1LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIiwiUkVBRCIsIlVQREFURSJdLCJ1c2VybmFtZSI6IjA5ODc5ODc5ODcifX0.HT2fMu3tSC2mZG_LU_Wap_nZ5mDIrhLcZyFWTbMd9A8', '2021-10-05 08:47:46'),
(32, NULL, 5, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzOTg4MjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkSVhVT0ExcE53XC9YZXhPUndaVkhudk9HcUxpT21nTFI5MmdtRFdoMTJzbnczNGtrOTFoZ1E2IiwidXNlcklkIjo1LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIiwiUkVBRCIsIlVQREFURSJdLCJ1c2VybmFtZSI6IjA5ODc5ODc5ODcifX0.CPfFE5t5ybIz3J666HIt9Fvash0i7wxY2E9AnV4_Y3I', '2021-10-05 08:53:46'),
(33, NULL, 10, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzOTkwODgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjEwLCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIiwiUkVBRCIsIlVQREFURSJdLCJ1c2VybmFtZSI6IjA5NzY2NTQzMjEifX0.qYdgYo97B2N41uQGG6Em7UlQJoT9Mv8hZlQOxgIk1KM', '2021-10-05 08:58:08'),
(34, NULL, 8, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzOTkxMzUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTGlRZkdZS1E3ZUxkSEtJWmhtbWJndXc0QjJjaDlwWWRFZGpMTVhCR0NlZGEzcHh0SWRmUjYiLCJ1c2VySWQiOjgsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiLCJSRUFEIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiMDk5NjY2Njc3NyJ9fQ.NmaajwJxsrG8H86IgnYK5fR2dqkvLsQorJAqsaT15wI', '2021-10-05 08:58:55'),
(35, NULL, 2, 0, NULL, NULL, 'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzMzOTkxNzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOUdEbVAzMXpwSWdtU1l5ODNBb2J3LmpVZW9YRXNmZDFWRU8udzZxdmJTVUNMNGhaeGRrOUciLCJ1c2VySWQiOjIsImF1dGhvcml0aWVzIjpbIlJFQUQiLCJERUxFVEUiLCJDUkVBVEUiLCJST0xFX0FETUlOIiwiVVBEQVRFIl0sInVzZXJuYW1lIjoiQWRtaW4ifX0.25v57_myDILA_TAWpV6EEsjV4ueg32VrT9tJ50nijwk', '2021-10-05 08:59:38');

-- --------------------------------------------------------

--
-- Table structure for table `t_user_role`
--

CREATE TABLE `t_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_user_role`
--

INSERT INTO `t_user_role` (`user_id`, `role_id`) VALUES
(2, 1),
(3, 2),
(4, 3),
(5, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKev1bd87g1c51ujncao608e6qa` (`customer_id`),
  ADD KEY `FK6uv0qku8gsu6x1r2jkrtqwjtn` (`product_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKpxtb8awmi0dk6smoh2vp1litg` (`customer_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKjyu2qbqt8gnvno9oe9j2s2ldk` (`order_id`),
  ADD KEY `FK4q98utpd73imf4yhttm3w0eax` (`product_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK45dp0030s8e3myd8n6ky4e79g` (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_922x4t23nx64422orei4meb2y` (`product_code`),
  ADD KEY `FK1eicg1yvaxh1gqdp2lsda7vlv` (`product_line_id`);

--
-- Indexes for table `product_lines`
--
ALTER TABLE `product_lines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKn0xus92n25hvud6dlfni0ttqg` (`comment_id`);

--
-- Indexes for table `t_permission`
--
ALTER TABLE `t_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_role`
--
ALTER TABLE `t_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_role_permission`
--
ALTER TABLE `t_role_permission`
  ADD PRIMARY KEY (`role_id`,`permission_id`),
  ADD KEY `FKjobmrl6dorhlfite4u34hciik` (`permission_id`);

--
-- Indexes for table `t_token`
--
ALTER TABLE `t_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user_role`
--
ALTER TABLE `t_user_role`
  ADD KEY `FKa9c8iiy6ut0gnx491fqx4pxam` (`role_id`),
  ADD KEY `FKcd59pwfr1yiqp8j029uau1ycw` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `product_lines`
--
ALTER TABLE `product_lines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_permission`
--
ALTER TABLE `t_permission`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_role`
--
ALTER TABLE `t_role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_token`
--
ALTER TABLE `t_token`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK6uv0qku8gsu6x1r2jkrtqwjtn` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKev1bd87g1c51ujncao608e6qa` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKpxtb8awmi0dk6smoh2vp1litg` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK4q98utpd73imf4yhttm3w0eax` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKjyu2qbqt8gnvno9oe9j2s2ldk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `FK45dp0030s8e3myd8n6ky4e79g` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK1eicg1yvaxh1gqdp2lsda7vlv` FOREIGN KEY (`product_line_id`) REFERENCES `product_lines` (`id`);

--
-- Constraints for table `replies`
--
ALTER TABLE `replies`
  ADD CONSTRAINT `FKn0xus92n25hvud6dlfni0ttqg` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`);

--
-- Constraints for table `t_role_permission`
--
ALTER TABLE `t_role_permission`
  ADD CONSTRAINT `FK90j038mnbnthgkc17mqnoilu9` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),
  ADD CONSTRAINT `FKjobmrl6dorhlfite4u34hciik` FOREIGN KEY (`permission_id`) REFERENCES `t_permission` (`id`);

--
-- Constraints for table `t_user_role`
--
ALTER TABLE `t_user_role`
  ADD CONSTRAINT `FKa9c8iiy6ut0gnx491fqx4pxam` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),
  ADD CONSTRAINT `FKcd59pwfr1yiqp8j029uau1ycw` FOREIGN KEY (`user_id`) REFERENCES `customers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
